import pickle
import cv2 as cv
import numpy as np
from math import degrees
from scipy.special import softmax

from data import Data

def prob_to_string(prob):   
    retval = "Neutral: {:.2f}\nHappy: {:.2f}\nSuprised: {:.2f}\nSad: {:.2f}\nAngry: {:.2f}\nDisgusted: {:.2f}\nAfraid: {:.2f}\nIn contempt: {:.2f}".format(*prob)
    return retval

def display(image, landmarks, scaling, rotation, prob):
    if landmarks is not None:
        rot_mat = cv.getRotationMatrix2D((rotation[0][0], rotation[0][1]), degrees(-rotation[1]), 1)
        image = cv.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv.INTER_LINEAR)
        
        for p in landmarks:
            p = (p[0] * -scaling[1], p[1] * -scaling[1])
            p = (p[0] + scaling[0][0], p[1] + scaling[0][1])
            p = (int(p[0]),int(p[1]))
            cv.circle(image, p, 2, (0,0,255),-1)
        
        cv.circle(image, (rotation[0][0], rotation[0][1]), 2, (0,255,0),-1)
        cv.circle(image, (int(scaling[0][0]), int(scaling[0][1])    ), 2, (255,0,0),-1)
        
        pred = np.argmax(prob)
        prob_list = prob_to_string(prob).split('\n')
        for i, text in enumerate(prob_list):
            image = cv.putText(image, text, (10,(i+1)*20), cv.FONT_HERSHEY_PLAIN, 1, (50, 255, 50) if i == pred else (50, 50, 255), 1, cv.LINE_4)

        cv.imshow('Display', image)

def classifier():
    data = Data('./data/', './data/shape_predictor_68_face_landmarks.dat')
    model = pickle.load(open('final_svm.model', 'rb'))

    stream = cv.VideoCapture(0)   
    while(True): 
        ret, frame = stream.read()     
        cv.imshow('stream', frame) 
        landmarks, scaling, rotation = data.retrieve_landmarks(frame)
        if landmarks is not None:
            prob = softmax(model.decision_function([np.reshape(landmarks, -1)])[0])
            pred = np.argmax(prob)
            display(frame, landmarks, scaling, rotation, prob)
            data.prediction_to_string(pred)
        if cv.waitKey(1) & 0xFF == ord('q'): 
            break
    stream.release() 
    cv.destroyAllWindows() 


if __name__ == "__main__":
    classifier()
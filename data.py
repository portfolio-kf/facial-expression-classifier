import csv
import cv2 as cv
import numpy as np
import dlib
import math
import enum

from numpy.lib.type_check import imag

class Record:
    image: np.array
    landmarks: np.array
    label: list
    scaling: tuple
    rotation: tuple

    def __init__(self, image, landmarks, label, scaling, rotation):
        self.image = image
        self.landmarks = landmarks
        self.label = label
        self.scaling = scaling
        self.rotation = rotation

class Data:
    '''
    Label vector: neutral, happiness, surprise, sadness, anger, disgust, fear, contempt, unknown, NF
    '''

    class Expression(enum.Enum):
        NEUTRAL = 0
        HAPPY = 1
        SUPRISED = 2
        SAD = 3
        ANGRY = 4
        DISGUSTED = 5
        AFRAID = 6
        IN_CONTEMPT = 7
        UNKNOWN = 8
        NOT_A_FACE = 9
    

    __data_path: str
    __detector: any
    __predictor: any
    __n_landmarks: int
    
    def __init__(self, data_path: str, predictor_path, n_landmarks=68):
        self.__data_path = data_path + '/'
        self.__detector = dlib.get_frontal_face_detector()
        self.__predictor = dlib.shape_predictor(predictor_path)
        self.__n_landmarks = n_landmarks

    def load_train(self, discard_images=False):
        path = self.__data_path + 'FER2013Train/'
        return self.__load_image_data(path, discard_images)

    def load_valid(self, discard_images=False):
        path = self.__data_path + 'FER2013Valid/'
        return self.__load_image_data(path, discard_images)

    def load_test(self, discard_images=False):
        path = self.__data_path + 'FER2013Test/'
        return self.__load_image_data(path, discard_images)

    def __load_image_data(self, path, discard_images=False):
        data = []
        label_fn = path + 'label.csv'
        with open(label_fn, 'r') as label_f:
            n_entries = len(label_f.readlines())
            print('0/{}\r'.format(n_entries), end='\r')
            label_f.seek(0)
            for i, line in enumerate(csv.reader(label_f)):
                img = cv.imread(path + line[0])
                label = np.array(line[2:], dtype='float')/10
                landmarks, scaling, rotation = self.retrieve_landmarks(img)
                if discard_images:
                    record = Record(None, landmarks, label, None, None)
                else:
                    record = Record(img, landmarks, label, scaling, rotation)
                data.append(record)
                if i % 250 == 0:
                    print('{}/{}'.format(i, n_entries), end='\r')
            print('{}/{}'.format(n_entries, n_entries))
        return data
        
    def retrieve_landmarks(self, img):
        dets = self.__detector(img, 1)
        if len(dets) <= 0:
            return None, None, None
        else:
            # Normalize landmarks: Position relative to central point
            d = dets[0]
            shape = self.__predictor(img, d)
            landmarks = [[shape.part(i).x, shape.part(i).y] for i in range(self.__n_landmarks)]

            nose_tip = landmarks[30]
            nose_angle = self.nose_angle(landmarks)

            # rotate vectors according to nose bridge
            rotated_landmarks = []
            for point in landmarks:
                rotated_landmarks.append(self.rotate_vector(nose_tip, point, nose_angle))

            # relative posititions in relation to center of gravity
            center = np.mean(np.array(rotated_landmarks), 0)
            relative_landmarks = []
            for point in rotated_landmarks:
                relative_landmarks.append((center[0]-point[0], center[1]-point[1]))

            max_value = np.max(np.abs(relative_landmarks), 0)
            max_magnitude = max_value.max()
            normalized_landmarks = np.array(relative_landmarks) / max_magnitude
            return normalized_landmarks, (center, max_magnitude), (nose_tip, nose_angle)

    @staticmethod
    def rotate_vector(center, point, angle):
        temp = (point[0] - center[0], point[1] - center[1])
        retval = (
            temp[0] * math.cos(angle) - temp[1] * math.sin(angle),
            temp[0] * math.sin(angle) + temp[1] * math.cos(angle)
        )
        return (retval[0] + center[0], retval[1] + center[1])

    @staticmethod
    def nose_angle(landmarks):
        """From Gent, P. (2016). Emotion Recognition Using Facial Landmarks, Python, DLib and OpenCV. A tech blog about fun things with Python and embedded electronics. Retrieved from: http://www.paulvangent.com/2016/08/05/emotion-recognition-using-facial-landmarks/

        Args:
            shape ([type]): [description]
        """
        if landmarks[27][0] == landmarks[30][0]:
            radians = 0
        else:
            radians = math.atan2(float(landmarks[30][0]-landmarks[27][0]),float(landmarks[30][1]-landmarks[27][1]))
            degrees = (radians *180) / math.pi
        return radians
        
    @staticmethod
    def svm_preprocessing(set: list):
        set_x = []; set_y = []
        for record in set:
            if record.landmarks is not None:
                # Majority voting and remove unknown and not a face
                selected_label = (record.label == record.label.max())[:8]
                selected_label = np.argwhere(selected_label).flatten()
                if len(selected_label) >= 3:
                    continue
                for i in selected_label:
                    set_x.append(np.reshape(record.landmarks, -1))
                    set_y.append(i) # Data.binary_label(i))
        return (set_x, set_y)

    @staticmethod
    def prediction_to_string(pred):
        results = ['Neutral', 'Happy', 'Suprised', 'Sad', 'Angry', 'Disgusted', 'Afraid', 'In contempt', 'Displaying unknown emotion', 'Not a face']
        return results[pred]
        
from data import Data
from sklearn import svm
import pickle
import numpy as np

if __name__ == "__main__":
    print('Loading data')
    data = Data('./data/', './data/shape_predictor_68_face_landmarks.dat')
    train_set = data.load_train(True)
    train_set.extend(data.load_valid(True))
    pp_train_set = Data.svm_preprocessing(train_set)

    print('Calculating weights')
    label_counts = np.bincount(pp_train_set[1])
    weights = label_counts.min() / label_counts
    weights_dict = {}
    for i in range(len(weights)):
        weights_dict[i] = weights[i]
    
    print('Start training')
    model = svm.SVC(kernel='rbf', class_weight=weights_dict,  gamma=6, C=60, tol=1e-3)
    model.fit(pp_train_set[0], pp_train_set[1])
    pickle.dump(model, open('./final_svm.model', 'wb'))
    
    print('Training completed')



from inspect import Parameter
from data import Data
from sklearn import svm
from sklearn.model_selection import KFold, GridSearchCV
import pickle
import numpy as np

if __name__ == "__main__":
    data = Data('./data/', './data/shape_predictor_68_face_landmarks.dat')
    
    train_set = data.load_train(True)
    train_set.extend(data.load_valid(True))
    
    pp_train_set = Data.svm_preprocessing(train_set)

    label_counts = np.bincount(pp_train_set[1])
    weights = label_counts.min() / label_counts
    weights_dict = {}
    for i in range(len(weights)):
        weights_dict[i] = weights[i]
    
    # params = [
    #     {
    #         'kernel': ['linear'],
    #         # 'C': [0.5, 1, 5, 10, 15, 20, 25, 50, 100],
    #         'tol': [1e-3],
    #         'class_weight': [weights_dict]
    #     },
    #     {
    #         'kernel': ['rbf'],
    #         'C': [0.5, 1, 5, 25, 100, 250, 500, 1000, 2500],
    #         'gamma': [1, 3, 5, 7],
    #         'tol': [1e-3],
    #         'class_weight': [weights_dict]
    #     },
    #     {
    #         'kernel': ['poly'],
    #         'C': [0.5, 1, 5, 25, 100, 250, 500, 1000, 2500],
    #         'degree': [3, 5, 7, 9],
    #         'tol': [1e-3],
    #         'class_weight': [weights_dict]
    #     }
    # ]

    params = [
        {
            'kernel': ['rbf'],
            'C': [50, 55, 60, 65, 70, 75, 80, 125, 150],
            'gamma': [4, 5, 6],
            'tol': [1e-3],
            'class_weight': [weights_dict]
        }
    ]
    
    svc = svm.SVC()
    clf = GridSearchCV(svc, params)
    clf.fit(pp_train_set[0], pp_train_set[1])
    print(clf.cv_results_)
    


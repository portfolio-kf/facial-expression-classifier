from data import Data

import pickle
from sklearn.metrics import accuracy_score, precision_recall_fscore_support

from matplotlib import pyplot as plt
from sklearn.metrics import plot_confusion_matrix

if __name__ == "__main__":
    data = Data('./data/', './data/shape_predictor_68_face_landmarks.dat')
    test_set = data.load_test(True)
    pp_test_set = data.svm_preprocessing(test_set)

    model = pickle.load(open('./final_svm.model', 'rb'))
    pred = model.predict(pp_test_set[0])
    acc = accuracy_score(pp_test_set[1], pred)
    precision, recall, f1, support = precision_recall_fscore_support(pp_test_set[1], pred, average=None)

    print('Accuracy: {}'.format(acc))
    print('Precision: {}'.format([float('{:.2f}'.format(p)) for p in precision]))
    print('Recall: {}'.format([float('{:.2f}'.format(r)) for r in recall]))
    print('F-Score: {}'.format([float('{:.2f}'.format(f)) for f in  f1]))
    
    plot = plot_confusion_matrix(model, pp_test_set[0], pp_test_set[1], normalize='true', display_labels=['Neutral', 'Happy', 'Suprised', 'Sad', 'Angry', 'Disgusted', 'Afraid', 'In contempt'], cmap=plt.cm.Blues)
    plot.ax_.set_title('Confusion matrix')

    plt.show()
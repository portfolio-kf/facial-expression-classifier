# Facial expression classifier

> Karel Fonteyn

This is a small project to predict facial expressions using a SVM and facial landmarks. A notebook is included which explains the used code. The code is also added as separate.

The performance of the classifier is evaluated using both a qualitative (live test environment included in project) and a quantitative evaluation (refer to the notebook for the results).



## What you need

1. Trained model used to detect facial landmarks (see http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2).
2. FER+ dataset (see https://github.com/microsoft/FERPlus).



## Structure

| Path                                 | Description                                                  |
| ------------------------------------ | ------------------------------------------------------------ |
| ./facial_expression_classifier.ipynb | Notebook                                                     |
| ./facial_expression_classifier.html  | Notebook as an HTML file                                     |
| ./data.py                            | Code to load and preprocess data                             |
| ./model_selection.py                 | Code to perform model selection                              |
| ./train_expression_classifier.py     | Code to train classifier                                     |
| ./test_expression_classifier.py      | Code to perform quantitative evaluation                      |
| ./live_expression_classifier.py      | Code to perform qualitative evaluation                       |
| ./data/                              | Directory structure for dataset (no data). <br />Should also contain model to detect facial landmarks. |
| ./content/                           | Directory containing images used in notebook.                |



## References


Barsoum, E., Zhang, C., Canton Ferrer, C., & Zhang, Z. (2016). Training Deep Networks for Facial Expression Recognition with Crowd-Sourced Label Distribution. In _ACM International Conference on Multimodal Interaction (ICMI)_.

Goodfellow, I.J., Erhan, D., Carrier, P.L., Courville, A.C., Mirza, M., Hamner, B., Cukierski, W., Tang, Y., Thaler, D., Lee, D., Zhou, Y., Ramaiah, C., Feng, F., Li, R., Wang, X., Athanasakis, D., Shawe-Taylor, J., Milakov, M., Park, J., Ionescu, R.T., Popescu, M., Grozea, C., Bergstra, J., Xie, J., Romaszko, L., Xu, B., Zhang, C., & Bengio, Y. (2015). Challenges in representation learning: A report on three machine learning contests. _Neural networks: the official journal of the International Neural Network Society_, 64, 59-63.

Van Gent, P. (2016, August 05). _Emotion Recognition using Facial Landmark, Python, DLib and OpenCV_. [https://theonly1.tistory.com/106]()

Swethan, R. (2018, March 08). _Emotion detection using facial landmarks and deep learning_. [https://medium.com/@rishiswethan.c.r/emotion-detection-using-facial-landmarks-and-deep-learning-b7f54fe551bf](https://medium.com/@rishiswethan.c.r/emotion-detection-using-facial-landmarks-and-deep-learning-b7f54fe551bf)